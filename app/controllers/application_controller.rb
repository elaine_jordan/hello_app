class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def hello
    render text: "Hello, world! How are you today??"
  end
  def goodbye
    render text: "Goodbye, world!"
  end
end
